y1=[0.146,0.120,0.281,0.395,0.504,0.583,0.668,0.729,0.810,0.849,0.926,0.964,1.012];
y2=[0.188,0.101,0.231,0.241,0.361,0.520,0.700,0.766,0.825,0.947,0.958,0.986,1.003];
erro=semilogy(abs(y1-y2),"linewidth",3,'-r');
set(erro,"linewidth",2)
set(gca, "linewidth", 2, "fontsize", 15)
xlabel('volume titulado de KSCN 0,1mol/L (mL)');
ylabel('Erro');
title('Gráfico de erro espectrofotômetro x fotômetro');
axis([0,14;0,1])
print -djpg graficoerro.jpg