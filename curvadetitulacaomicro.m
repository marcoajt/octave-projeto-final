y1=[0.146,0.120,0.281,0.395,0.504,0.583,0.668,0.729,0.810,0.849,0.926,0.964,1.012];
y2=[0.188,0.101,0.231,0.241,0.361,0.520,0.700,0.766,0.825,0.947,0.958,0.986,1.003];
absorbancia=plot(y1,'+r',y2,'*k');
set(absorbancia,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid on;
xlabel('volume titulado de KSCN 0,1mol/L (mL)');
ylabel('Absorb�ncia');
legend("espectrofot�metro", "fot�metro")
title('Curva de titula��o');
axis([0,14;0,y1])
print -djpg curva.jpg
